/*
 * @lc app=leetcode.cn id=1 lang=golang
 *
 * [1] 两数之和
 */

// @lc code=start
/*
func twoSum(nums []int, target int) []int {
	for i := 0;i<len(nums);i++{
		for j := i+ 1;j<len(nums);j++{ 
			if nums[i]+nums[j] == target{
				return []int {i,j}
			}
		}
	}
	return nil;
}
*/
func twoSum(nums []int,target int) []int{
	hashTable := map[int]int{}
	for i,x := range nums{
		if p, ok := hashTable[target-x];ok{
			return []int{p,i}
		}
		hashTable[x] = i
	}
	return nil
}
// @lc code=end

