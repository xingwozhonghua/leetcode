/*
 * @lc app=leetcode.cn id=1 lang=javascript
 *
 * [1] 两数之和
 */

// @lc code=start
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
var twoSum = function (nums, target) {
    var obj = {}
    for (var i = 0; i < nums.length - 1; i++) {
        obj.a = i
        for (var j = i + 1; j < nums.length; j++) {
            obj.b = j
            if (nums[i] + nums[j] == target) {
                return Object.values(obj)
            }
        }
    }
};
// @lc code=end

